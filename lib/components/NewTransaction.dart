import 'package:flutter/material.dart';

final titleController = TextEditingController();
final amountController = TextEditingController();


class NewTransaction extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            TextField(
              decoration: InputDecoration(
                  labelText: 'Título'
              ),
              //onChanged: (value) => titleInput = value,
              controller: titleController,
            ),
            TextField(
              decoration: InputDecoration(
                  labelText: 'Total'
              ),
              //onChanged: (value) => amoundInput = value,
              controller: amountController,
            ),
            FlatButton(
              child: Text('Adicionar'),
              onPressed: () {

              },
              textColor: Colors.purple,
            )
          ],
        ),
      ),
    );
  }
}

import 'package:expenses_app/components/NewTransaction.dart';
import 'package:expenses_app/components/TransactionList.dart';
import 'package:expenses_app/models/transaction.dart';
import 'package:flutter/material.dart';

class UserTransactions extends StatefulWidget {
  @override
  _UserTransactionsState createState() => _UserTransactionsState();
}

class _UserTransactionsState extends State<UserTransactions> {

  final List<Transaction> _userTransactions = [
    Transaction(
      id: 't1',
      title: 'Yakisoba',
      amount: 12,
      date: DateTime.now(),
    ),
    Transaction(
      id: 't2',
      title: 'Coca Cola',
      amount: 4,
      date: DateTime.now(),
    )
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        NewTransaction(),
        TransactionList(_userTransactions),
      ],
    );
  }
}
